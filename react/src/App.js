import logo from './logo.svg';
import './App.css';
import React, {useState} from 'react';

function TextInput({ addText}) {
  const [value, setValue] = useState();
  
  const handleSubmit = e => {
      e.preventDefault();
      if (!value) return;
      addText(value);
      setValue("");
  };
  
  return (
      <form onSubmit={handleSubmit}>
      <input
          type="text"
          className="input"
          value={value}
          onChange={e => setValue(e.target.value)}
      />
      </form>
  );
}


function App() {
  const [text, setText] = useState([]);

  const addText = text => {
    setText(text);
  };

  function titleCase(string) {
    var sentence = string.toLowerCase().split(" ");
    for(var i = 0; i< sentence.length; i++){
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }
    document.write(sentence.join(" "));
    return sentence;
  }


  return (
    <div className="App">
      <header className="App-header">
        <TextInput addText = {addText}/>
        <br/>
        <br/>
        <br/>
        <div>
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
          </ul>
        </div>
      </header>
    </div>
  );
}

export default App;
