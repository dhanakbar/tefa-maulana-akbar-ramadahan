const values = {
  name: "John Doe",
  age: 21,
  username: "johndoe",
  password: "abc123",
  location: "Jakarta",
  time: new Date(),
}

// Using filter
const filtered = Object.fromEntries(Object.entries(values).filter(([key]) => key.includes('name')));

// Using delete
delete values.password;
delete values.time;
console.log(JSON.stringify(values))